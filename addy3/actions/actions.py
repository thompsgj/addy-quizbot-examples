from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk.events import SlotSet, AllSlotsReset

class ValidateBeginnerQuizForm(FormValidationAction):
    def __init__(self):
        self.score = 0

    def name(self) -> Text:
        return "validate_beginner_quiz_form"

    @staticmethod
    def beginnerQ1_db() -> List[Text]:
        """Database of supported responses for beginner Q1"""

        return ["2", "two"]

    @staticmethod
    def beginnerQ2_db() -> List[Text]:
        """Database of supported responses for beginner Q2"""

        return ["11", "eleven"]

    def common_validation(self, slot, val, db, score):
        if val.lower() in db:
            self.score += 1

            return {slot: val, "score": self.score}
        else:
            return {slot: val}

    def validate_beginnerQ1(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ1_db()
        slot = "beginnerQ1"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ2(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ2_db()
        slot = "beginnerQ2"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

class ValidateAdvancedQuizForm(FormValidationAction):
    def __init__(self):
        self.score = 0

    def name(self) -> Text:
        return "validate_advanced_quiz_form"

    @staticmethod
    def advancedQ1_db() -> List[Text]:
        """Database of supported responses for advanced Q1"""

        return ["873", "eight hundred seventy-three"]

    @staticmethod
    def advancedQ2_db() -> List[Text]:
        """Database of supported responses for advanced Q2"""

        return ["444", "four hundred forty-four"]

    def common_validation(self, slot, val, db, score):
        if val.lower() in db:
            self.score += 1

            return {slot: val, "score": self.score}
        else:
            return {slot: val}

    def validate_advancedQ1(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ1_db()
        slot = "advancedQ1"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ2(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ2_db()
        slot = "advancedQ2"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update



class ResetForms(Action):
    def name(self):
        return "reset_forms"

    def run(self, dispatcher, tracker, domain):
        name = tracker.get_slot('name')
        quiz_level = tracker.get_slot('quiz_level')
        score = tracker.get_slot('score')
        return [AllSlotsReset(), SlotSet("name", name), SlotSet("quiz_level", quiz_level), SlotSet("score", score)]

class ResetScore(Action):
    def name(self):
        return "reset_score"

    def run(self, dispatcher, tracker, domain):
        score = tracker.get_slot('score')
        quiz_retake = tracker.get_slot('quiz_retake')
        return[SlotSet("score", 0), SlotSet("quiz_retake", '')]
